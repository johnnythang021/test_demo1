
const express = require("express");

const app = express();

app.get("/",(req,res)=>{
    res.send({hello: "world"});
});
app.get("/sum",(req,res)=>{
	let num1=parseInt(req.query.num1);
	let num2=parseInt(req.query.num2);
	let rs= num1+num2;
	res.send({sum: rs});
})

const PORT = process.env.PORT || 5000;
app.listen(PORT , function(){
    console.log(`app listening ${PORT}`);
});
